/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.policy;

import org.eclipse.gef.EditPolicy;
import org.eclipse.graphiti.ui.internal.config.AbstractConfigurationProviderHolder;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderInternal;
import org.eclipse.graphiti.ui.platform.IConfigurationProvider;

/**
 * A default implementation of the interface IEditPolicyFactory.
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class DefaultEditPolicyFactory extends AbstractConfigurationProviderHolder implements IEditPolicyFactory {

	/**
	 * Creates a new DefaultEditPolicyFactory.
	 * 
	 * @param configurationProvider
	 *            The IConfigurationProviderInternal, to which this factory belongs.
	 */
	public DefaultEditPolicyFactory(IConfigurationProviderInternal configurationProvider) {
		super(configurationProvider);
	}

	public void dispose() {
	}

	public EditPolicy createShapeForbidLayoutEditPolicy() {
		return new ShapeForbidLayoutEditPolicy(getConfigurationProvider());
	}

	public EditPolicy createShapeXYLayoutEditPolicy() {
		// return new ShapeXYLayoutEditPolicy(getConfigurationProvider().getCommandFactory());
		return new ShapeContainerAndXYLayoutEditPolicy(getConfigurationProvider());
	}

	public EditPolicy createShapeHighlightEditPolicy() {
		return new ShapeHighlightEditPolicy(getConfigurationProvider());
	}

	public static String HOVER_POLICY_KEY = "hover"; //$NON-NLS-1$

	public EditPolicy createShapeHoverEditPolicy() {
		return new ShapeHoverEditPolicy(getConfigurationProvider());
	}

	public EditPolicy createConnectionHighlightEditPolicy() {
		return new ConnectionHighlightEditPolicy(getConfigurationProvider());
	}

	public EditPolicy createConnectionEditPolicy() {
		return new GFNodeEditPolicy(getConfigurationProvider());
	}

	public EditPolicy createDirectEditPolicy() {
		return new DefaultDirectEditPolicy(getConfigurationProvider());
	}

	public EditPolicy createModelObjectDeleteEditPolicy(IConfigurationProvider configurationProvider) {
		return new ModelObjectDeleteEditPolicy(configurationProvider);
	}

	public EditPolicy createConnectionBendpointsEditPolicy() {
		return new ConnectionBendpointEditPolicy(getConfigurationProvider());
		// return new MoveBendpointEditPolicy();
	}

	public EditPolicy createConnectionDeleteEditPolicy(IConfigurationProvider configurationProvider) {
		return new DefaultConnectionEditPolicy(configurationProvider);
	}

}