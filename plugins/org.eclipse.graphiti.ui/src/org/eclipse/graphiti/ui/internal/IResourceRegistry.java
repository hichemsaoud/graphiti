/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    palldredge - Bug 465675 - Improve SWT Font management 
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;

/**
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public interface IResourceRegistry extends IDisposable {
	/**
	 * Provides a SWT color instance with the given RGB values.
	 * 
	 * @param red the value for the red part of the color
	 * @param green the value for the green part of the color
	 * @param blue the value for the blue part of the color
	 * 
	 * @return a SWT color instance
	 * 
	 * @see org.eclipse.swt.graphics.Color
	 */
	Color getSwtColor(int red, int green, int blue);

	/**
	 * @param hexRGBString

	 * @return a SWT color instance
	 * 
	 * @see org.eclipse.swt.graphics.Color
	 */
	Color getSwtColor(String hexRGBString);

	/**
	 * Provides an SWT font instance with the given font data.
	 * 
	 * @param fontData the font data describing the desired font
	 * @return an SWT font instance
	 * 
	 * @see org.eclipse.swt.graphics.Font
	 */
	Font getSwtFont(FontData fontData);
}