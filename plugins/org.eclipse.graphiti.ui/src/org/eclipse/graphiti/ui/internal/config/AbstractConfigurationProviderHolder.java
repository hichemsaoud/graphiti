/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.config;

/**
 * A simple implementation of the interface IConfigurationProviderHolder.
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public abstract class AbstractConfigurationProviderHolder implements IConfigurationProviderHolder {

	private IConfigurationProviderInternal _configurationProvider;

	/**
	 * Creates a new SimpleConfigurationProviderHolder.
	 * 
	 * @param configurationProvider
	 *            the configuration provider
	 */
	public AbstractConfigurationProviderHolder(IConfigurationProviderInternal configurationProvider) {
		if (configurationProvider == null)
			throw new RuntimeException("Implementation-error: the IConfigurationProviderInternal must not be null."); //$NON-NLS-1$

		_configurationProvider = configurationProvider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.graphiti.ui.config.provider.IConfigurationProviderHolder#
	 * getConfigurationProvider()
	 */
	public IConfigurationProviderInternal getConfigurationProvider() {
		return _configurationProvider;
	}
}