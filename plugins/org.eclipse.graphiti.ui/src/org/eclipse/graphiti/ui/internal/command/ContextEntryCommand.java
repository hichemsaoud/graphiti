/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.command;

import org.eclipse.gef.commands.Command;
import org.eclipse.graphiti.tb.IContextEntry;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class ContextEntryCommand extends Command {

	private IContextEntry contextEntry;

	public ContextEntryCommand(IContextEntry contextEntry) {
		setContextEntry(contextEntry);
	}

	@Override
	public void execute() {
		getContextEntry().execute();
		super.execute();
	}

	private void setContextEntry(IContextEntry contextEntry) {
		this.contextEntry = contextEntry;
	}

	public IContextEntry getContextEntry() {
		return contextEntry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#getLabel()
	 */
	@Override
	public String getLabel() {
		return getContextEntry().getText();
	}

}
