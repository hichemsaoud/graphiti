/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.services;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;

/**
 * Services regarding EMF.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 * @since 0.9
 */
public interface IEmfService {
	
	/**
	 * Creates a {@link TransactionalEditingDomain} with a {@link ResourceSet}
	 * resource set and a {@link IWorkspaceCommandStack} command stack.
	 * 
	 * @return a {@link TransactionalEditingDomain} editing domain
	 */
	public abstract TransactionalEditingDomain createResourceSetAndEditingDomain();

}
