/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.mm.pictograms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manhattan Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.graphiti.mm.pictograms.PictogramsPackage#getManhattanConnection()
 * @model
 * @generated
 */
public interface ManhattanConnection extends Connection {
} // ManhattanConnection
