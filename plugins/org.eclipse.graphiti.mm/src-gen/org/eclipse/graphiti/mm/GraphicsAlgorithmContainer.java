/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.mm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graphics Algorithm Container</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.graphiti.mm.MmPackage#getGraphicsAlgorithmContainer()
 * @model abstract="true"
 * @generated
 */
public interface GraphicsAlgorithmContainer extends PropertyContainer {
} // GraphicsAlgorithmContainer
