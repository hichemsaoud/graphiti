package org.eclipse.graphiti.export.batik.internal;

import org.apache.batik.ext.awt.image.spi.ImageTagRegistry;
import org.apache.batik.ext.awt.image.spi.ImageWriterRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class GraphitiBatikPlugin extends AbstractUIPlugin {

	public GraphitiBatikPlugin() {
		super();
		registerBatikImageFormats();
	}

	private void registerBatikImageFormats() {
		ImageWriterRegistry.getInstance()
				.register(new org.apache.batik.ext.awt.image.codec.imageio.ImageIOPNGImageWriter());
		ImageWriterRegistry.getInstance()
				.register(new org.apache.batik.ext.awt.image.codec.imageio.ImageIOTIFFImageWriter());
		ImageWriterRegistry.getInstance()
				.register(new org.apache.batik.ext.awt.image.codec.imageio.ImageIOJPEGImageWriter());

		ImageTagRegistry.getRegistry().register(new org.apache.batik.ext.awt.image.codec.png.PNGRegistryEntry());
		ImageTagRegistry.getRegistry()
				.register(new org.apache.batik.ext.awt.image.codec.imageio.ImageIOJPEGRegistryEntry());
		ImageTagRegistry.getRegistry()
				.register(new org.apache.batik.ext.awt.image.codec.imageio.ImageIOPNGRegistryEntry());
		ImageTagRegistry.getRegistry()
				.register(new org.apache.batik.ext.awt.image.codec.imageio.ImageIOTIFFRegistryEntry());
	}
}
