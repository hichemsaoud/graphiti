/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 325084 - Provide documentation for Patterns
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.pattern.mapping;

import java.util.Collections;
import java.util.List;

import org.eclipse.graphiti.features.IMappingProvider;
import org.eclipse.graphiti.pattern.mapping.data.IDataMapping;



/**
 * The Class StructureMappingMulti.
 * 
 * @experimental This API is in an experimental state and should be used by
 *               clients, as it not final and can be removed or changed without
 *               prior notice!
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @noinstantiate This class is not intended to be instantiated by clients.
 */
public abstract class StructureMappingMulti extends StructureMapping implements IStructureMappingMulti {

	/**
	 * Creates a new {@link StructureMappingMulti}.
	 * 
	 * @param dataMapping
	 *            the data mapping
	 * @param mappingProvider
	 *            the mapping provider
	 */
	public StructureMappingMulti(IDataMapping dataMapping, IMappingProvider mappingProvider) {
		super(dataMapping, mappingProvider);
	}

	public List<ILinkCreationInfo> getLinkCreationInfos(Object mainBusinessObject) {
		ILinkCreationInfo linkCreationInfo = new LinkCreationInfo(mainBusinessObject);
		return Collections.singletonList(linkCreationInfo);
	}
}
