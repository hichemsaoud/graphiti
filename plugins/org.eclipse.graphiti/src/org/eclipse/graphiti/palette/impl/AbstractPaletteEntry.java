/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.palette.impl;

import org.eclipse.graphiti.palette.IPaletteEntry;

/**
 * The Class AbstractPaletteEntry.
 */
public abstract class AbstractPaletteEntry implements IPaletteEntry {

	private String label, iconId;

	/**
	 * Creates a new {@link AbstractPaletteEntry}.
	 * 
	 * @param label
	 *            the label
	 * @param iconId
	 *            the icon id
	 */
	public AbstractPaletteEntry(String label, String iconId) {
		this.label = label;
		this.iconId = iconId;
	}

	public String getLabel() {
		return this.label;
	}

	public String getIconId() {
		return this.iconId;
	}

}
