/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.IMoveContext;

/**
 * The Class MoveContext.
 */
public class MoveContext extends LocationContext implements IMoveContext {

	/**
	 * Creates a new {@link MoveContext}.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public MoveContext(int x, int y) {
		super(x, y);
	}

	/**
	 * Instantiates a new move context.
	 */
	public MoveContext() {
	}
}
