/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context.impl;

import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.internal.features.context.impl.base.DetailedPictogramElementContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Class DirectEditingContext.
 */
public class DirectEditingContext extends DetailedPictogramElementContext implements IDirectEditingContext {

	/**
	 * Creates a new {@link DirectEditingContext}.
	 * 
	 * @param pictogramElement
	 *            the pictogram element
	 * @param graphicsAlgorithm
	 *            the graphics algorithm
	 */
	public DirectEditingContext(PictogramElement pictogramElement, GraphicsAlgorithm graphicsAlgorithm) {
		super(pictogramElement, graphicsAlgorithm);

	}

}
