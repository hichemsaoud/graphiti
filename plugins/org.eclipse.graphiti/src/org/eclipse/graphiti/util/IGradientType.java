/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.util;

import org.eclipse.graphiti.mm.algorithms.styles.AdaptedGradientColoredAreas;

/**
 * The constants provided by this interface can be used with
 * {@link AdaptedGradientColoredAreas}. If <i>VERTICAL</i> is set, the gradient
 * will go from top to bottom. If <i>HORIZONTAL</i>, it will go from left to
 * right.
 */
public interface IGradientType {

	public final Integer VERTICAL = 0;

	public final Integer HORIZONTAL = 1;

}
