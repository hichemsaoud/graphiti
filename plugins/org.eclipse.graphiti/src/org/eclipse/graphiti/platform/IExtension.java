/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.platform;

/**
 * The Interface IExtension.
 */
public interface IExtension {

	/**
	 * Gets the provider id.
	 * 
	 * @return the provider id
	 */
	String getProviderId();

	/**
	 * Sets the provider id.
	 * 
	 * @param providerId
	 *            the new provider id
	 */
	void setProviderId(String providerId);
}
