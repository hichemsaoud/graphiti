/*********************************************************************
* Copyright (c) 2005, 2022 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 323155 - Check usage scenarios for DefaultPrintFeature and
*            DefaultSaveImageFeature
*    cbrand - Bug 376585 - Clean-up deprecations in Graphiti
*    Philip Alldredge - Bug 418676 - Undo is not disabled when canUndo is false for Palette features
*    mwenz - Bug 423573 - Angles should never be integer
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Vector;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.datatypes.IDimension;
import org.eclipse.graphiti.datatypes.ILocation;
import org.eclipse.graphiti.datatypes.IRectangle;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IDirectEditingFeature;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IMappingProvider;
import org.eclipse.graphiti.features.IMoveShapeFeature;
import org.eclipse.graphiti.features.IResizeShapeFeature;
import org.eclipse.graphiti.features.context.IAreaContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.context.impl.AddBendpointContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.AreaAnchorContext;
import org.eclipse.graphiti.features.context.impl.AreaContext;
import org.eclipse.graphiti.features.context.impl.CreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.CreateContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.context.impl.DirectEditingContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.context.impl.LocationContext;
import org.eclipse.graphiti.features.context.impl.MoveBendpointContext;
import org.eclipse.graphiti.features.context.impl.MoveConnectionDecoratorContext;
import org.eclipse.graphiti.features.context.impl.MoveContext;
import org.eclipse.graphiti.features.context.impl.MoveShapeContext;
import org.eclipse.graphiti.features.context.impl.PrintContext;
import org.eclipse.graphiti.features.context.impl.ReconnectionContext;
import org.eclipse.graphiti.features.context.impl.RemoveBendpointContext;
import org.eclipse.graphiti.features.context.impl.ResizeContext;
import org.eclipse.graphiti.features.context.impl.ResizeShapeContext;
import org.eclipse.graphiti.features.context.impl.SaveImageContext;
import org.eclipse.graphiti.features.context.impl.SplitConnectionContext;
import org.eclipse.graphiti.internal.ExternalPictogramLink;
import org.eclipse.graphiti.internal.command.CommandContainer;
import org.eclipse.graphiti.internal.command.DirectEditingFeatureCommandWithContext;
import org.eclipse.graphiti.internal.command.ICommand;
import org.eclipse.graphiti.internal.command.MoveShapeFeatureCommandWithContext;
import org.eclipse.graphiti.internal.command.ResizeShapeFeatureCommandWithContext;
import org.eclipse.graphiti.internal.datatypes.impl.DimensionImpl;
import org.eclipse.graphiti.internal.datatypes.impl.LocationImpl;
import org.eclipse.graphiti.internal.datatypes.impl.RectangleImpl;
import org.eclipse.graphiti.mm.StyleContainer;
import org.eclipse.graphiti.mm.algorithms.AbstractText;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.PlatformGraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.algorithms.styles.RenderingStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Style;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.palette.impl.StackEntry;
import org.eclipse.graphiti.platform.IPlatformImageConstants;
import org.eclipse.graphiti.platform.ga.RendererContext;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.tb.ContextButtonEntry;
import org.eclipse.graphiti.tb.ContextEntryHelper;
import org.eclipse.graphiti.tb.ContextMenuEntry;
import org.eclipse.graphiti.tb.DynamicContextMenuEntry;
import org.eclipse.graphiti.tb.IContextEntry;
import org.eclipse.graphiti.tb.ImageDecorator;
import org.eclipse.graphiti.tests.reuse.GFAbstractTestCase;
import org.eclipse.graphiti.ui.internal.command.AddModelObjectCommand;
import org.eclipse.graphiti.ui.internal.command.ContextEntryCommand;
import org.eclipse.graphiti.ui.internal.command.CreateModelObjectCommand;
import org.eclipse.graphiti.ui.internal.command.GFCommand;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderInternal;
import org.eclipse.graphiti.ui.internal.parts.directedit.IDirectEditHolder;
import org.eclipse.graphiti.ui.internal.requests.ContextButtonDragRequest;
import org.eclipse.graphiti.ui.internal.requests.GFDirectEditRequest;
import org.eclipse.graphiti.ui.internal.util.DataTypeTransformation;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.graphics.FontData;
import org.junit.Test;

/**
 *
 */
public class PackageTest extends GFAbstractTestCase {

	private static final String DUMMY = "dummy";

	@Test
	public void testInternalRequests() throws Exception {
		// test
		GFDirectEditRequest myDirectEditRequest = new GFDirectEditRequest();

		IDirectEditHolder directEditHolderMock = mock(IDirectEditHolder.class);

		myDirectEditRequest.setDirectEditingContext(directEditHolderMock);

		IDirectEditHolder directEditHolder = myDirectEditRequest.getDirectEditHolder();
		assertEquals(directEditHolderMock, directEditHolder);

		// test
		ContextButtonDragRequest myContextButtonDragRequest = new ContextButtonDragRequest();

		ContextButtonEntry contextButtonEntry = new ContextButtonEntry(mock(IFeature.class), mock(IContext.class));
		contextButtonEntry.addDragAndDropFeature(null);
		contextButtonEntry.getDragAndDropFeatures();
		contextButtonEntry.addContextButtonMenuEntry(contextButtonEntry);
		contextButtonEntry.getContextButtonMenuEntries();

		myContextButtonDragRequest.setContextButtonEntry(contextButtonEntry);

		ContextButtonEntry contextButtonEntry2 = myContextButtonDragRequest.getContextButtonEntry();
		assertEquals(contextButtonEntry, contextButtonEntry2);
	}

	@Test
	public void testCommand() throws Exception {
		ICommand commandMock = mock(ICommand.class);

		// Connection connectionMock = mock(Connection.class);
		// replay(connectionMock);

		// #########################################################

		IMoveShapeFeature moveShapeFeatureMock = mock(IMoveShapeFeature.class);

		// IMoveShapeContext moveShapeContextMock =
		// mock(IMoveShapeContext.class);
		// when(moveShapeContextMock.get)
		// replay(moveShapeContextMock);
		Shape shapeMock = mock(Shape.class);

		MoveShapeContext moveShapeContext = new MoveShapeContext(shapeMock);
		moveShapeContext.setLocation(10, 10);
		moveShapeContext.getShape();
		moveShapeContext.getTargetConnection();
		moveShapeContext.getPictogramElement();
		moveShapeContext.getDeltaX();
		moveShapeContext.getDeltaY();
		moveShapeContext.getSourceContainer();
		moveShapeContext.getTargetConnection();
		moveShapeContext.getTargetContainer();
		moveShapeContext.getX();
		moveShapeContext.getY();

		MoveShapeFeatureCommandWithContext myMoveShapeFeatureCommandWithContext = new MoveShapeFeatureCommandWithContext(
				moveShapeFeatureMock, moveShapeContext);

		myMoveShapeFeatureCommandWithContext.canExecute();
		myMoveShapeFeatureCommandWithContext.execute();

		// #########################################################

		// testorg.eclipse.graphiti.command.CommandContainer
		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);

		commandMock = mock(ICommand.class);

		CommandContainer myCommandContainer = new CommandContainer(featureProviderMock);

		myCommandContainer.add(myMoveShapeFeatureCommandWithContext);
		myCommandContainer.add(commandMock);

		myCommandContainer.getCommands();
		myCommandContainer.getDescription();
		myCommandContainer.getFeatureProvider();
		assertTrue(myCommandContainer.containsCommands());
		myCommandContainer.canExecute();
		myCommandContainer.undo();
		myCommandContainer.execute();
		myCommandContainer.undo();

		// #########################################################

		// testorg.eclipse.graphiti.command.ResizeShapeFeatureCommandWithContext
		IResizeShapeContext resizeShapeContextMock = mock(IResizeShapeContext.class);

		IFeature featureMock = mock(IResizeShapeFeature.class);

		ResizeShapeFeatureCommandWithContext myResizeShapeFeatureCommandWithContext = new ResizeShapeFeatureCommandWithContext(featureMock,
				resizeShapeContextMock);

		myResizeShapeFeatureCommandWithContext.canExecute();
		myResizeShapeFeatureCommandWithContext.execute();

		// #########################################################

		IDirectEditingContext directEditingContextMock = mock(IDirectEditingContext.class);

		IDirectEditingFeature directEditingFeatureMock = mock(IDirectEditingFeature.class);

		DirectEditingFeatureCommandWithContext myDirectEditingFeatureCommandWithContext = new DirectEditingFeatureCommandWithContext(
				directEditingFeatureMock, directEditingContextMock, new String("Value"),null);

		myDirectEditingFeatureCommandWithContext.execute();
	}

	@Test
	public void testDatatypesImpl() throws Exception {

		// test DimensionImpl
		final int WIDTH = 30;
		final int HEIGHT = 20;
		final int WIDTH_DIFF = 10;
		final int HEIGHT_DIFF = 5;
		final int WIDTH_2 = WIDTH + WIDTH_DIFF;
		final int HEIGHT_2 = HEIGHT + HEIGHT_DIFF;

		DimensionImpl d1 = new DimensionImpl(WIDTH, HEIGHT);
		assertEquals(WIDTH, d1.getWidth());
		assertEquals(HEIGHT, d1.getHeight());

		d1.hashCode();

		DimensionImpl d2 = new DimensionImpl(d1);
		assertEquals(WIDTH, d2.getWidth());
		assertEquals(HEIGHT, d2.getHeight());

		d2.scale(2);
		assertEquals(2 * WIDTH, d2.getWidth());
		assertEquals(2 * HEIGHT, d2.getHeight());

		d2.setDimension(WIDTH, HEIGHT);
		assertEquals(WIDTH, d2.getWidth());
		assertEquals(HEIGHT, d2.getHeight());

		d2.expand(WIDTH_DIFF, HEIGHT_DIFF);
		assertEquals(WIDTH_2, d2.getWidth());
		assertEquals(HEIGHT_2, d2.getHeight());

		d2.setDimension(d1);
		assertEquals(WIDTH, d2.getWidth());
		assertEquals(HEIGHT, d2.getHeight());

		IDimension d3 = d1.getDimensionCopy();
		assertTrue(d1.equals(d1));
		assertTrue(d1.equals(d3));
		assertFalse(d1.equals(DUMMY));

		// #########################################################

		// test LocationImpl
		final int X = 15;
		final int Y = 25;
		final int DX = 5;
		final int DY = 10;
		final int X2 = X + DX;
		final int Y2 = Y + DY;

		LocationImpl l1 = new LocationImpl(X, Y);
		assertEquals(X, l1.getX());
		assertEquals(Y, l1.getY());

		l1.hashCode();
		l1.toString();

		LocationImpl l2 = new LocationImpl(l1);
		assertEquals(X, l2.getX());
		assertEquals(Y, l2.getY());

		l2.scale(2);
		assertEquals(2 * X, l2.getX());
		assertEquals(2 * Y, l2.getY());

		l2.setLocation(X, Y);
		assertEquals(X, l2.getX());
		assertEquals(Y, l2.getY());

		l2.translate(DX, DY);
		assertEquals(X2, l2.getX());
		assertEquals(Y2, l2.getY());

		l2.setLocation(l1);
		assertEquals(X, l2.getX());
		assertEquals(Y, l2.getY());

		ILocation l3 = l1.getLocationCopy();
		assertTrue(l1.equals(l1));
		assertTrue(l1.equals(l3));
		assertFalse(l1.equals(DUMMY));

		// #########################################################

		// test LocationImpl
		RectangleImpl r1 = new RectangleImpl(WIDTH, HEIGHT);
		// RectangleImpl r2 = new RectangleImpl(r1);

		r1.hashCode();
		r1.toString();

		r1.contains(l3);

		r1.expand(WIDTH_DIFF, HEIGHT_DIFF);
		r1.scale(2);
		r1.translate(DX, DY);

		IDimension d4 = r1.getDimensionCopy();
		ILocation l4 = r1.getLocationCopy();
		IRectangle r2 = r1.getRectangleCopy();

		r1.setDimension(d4);
		r1.setDimension(WIDTH, HEIGHT);

		r1.setLocation(l4);
		r1.setLocation(0, 0);

		r1.setRectangle(r2);

		assertTrue(r1.equals(r1));
		assertTrue(r1.equals(r2));
		assertFalse(r1.equals(DUMMY));
	}

	@Test
	public void testTb() throws Exception {

		// ContextButtonEntry and ContextEntryHelper
		ContextButtonEntry cbe = new ContextButtonEntry(null, null);
		ContextEntryHelper.markAsUpdateContextEntry(cbe);
		ContextEntryHelper.markAsCollapseContextEntry(cbe, true);
		ContextEntryHelper.markAsCollapseContextEntry(cbe, false);
		ContextEntryHelper.createCollapseContextButton(false, null, null);

		// ImageRenderingDecorator
		final int X = 10;
		final int Y = 20;

		ImageDecorator rd = new ImageDecorator(IPlatformImageConstants.IMG_ECLIPSE_ERROR);
		rd.setX(X);
		rd.setY(Y);
		rd.setMessage(DUMMY);

		rd.getX();
		rd.getY();
		rd.getMessage();
		rd.getImageId();

		// DynamicContextMenuEntry
		DynamicContextMenuEntry cme = new DynamicContextMenuEntry(null, null);
		cme.setText(DUMMY);
		cme.setMinimumSubmenuEntries(5);
		cme.add(new ContextMenuEntry(null, null));
		assertFalse(cme.isSubmenu());
	}

	@Test
	public void testUiInternalCommand() throws Exception {
		// test
		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);

		IDiagramTypeProvider diagramTypeProviderMock = mock(IDiagramTypeProvider.class);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(null);

		IConfigurationProviderInternal configurationProviderMock = mock(IConfigurationProviderInternal.class);
		when(configurationProviderMock.getDiagramTypeProvider()).thenReturn(diagramTypeProviderMock);

		ContainerShape containerShapeMock = mock(ContainerShape.class);

		IAdaptable adaptableMock = mock(IAdaptable.class);
		// RefObject aRefObject = new ContainerShapeImpl();
		// when(adaptableMock.getAdapter(RefObject.class)).thenReturn(aRefObject)
		// ;
		when(adaptableMock.getAdapter(EObject.class)).thenReturn(containerShapeMock);

		ISelection selection = new StructuredSelection(new Object[] { adaptableMock });

		Rectangle rectangle = new Rectangle();

		AddModelObjectCommand myAddModelObjectCommand = new AddModelObjectCommand(configurationProviderMock, containerShapeMock, selection,
				rectangle);


		myAddModelObjectCommand.canExecute();

		reset(diagramTypeProviderMock);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(featureProviderMock);

		myAddModelObjectCommand.canExecute();

		myAddModelObjectCommand.execute();

		assertTrue(myAddModelObjectCommand.canUndo());

		IContextEntry contextEntryMock = mock(IContextEntry.class);

		ContextEntryCommand myContextEntryCommand = new ContextEntryCommand(contextEntryMock);

		myContextEntryCommand.execute();

		// test
		GFCommand myGraphitiCommand = new GFCommand(configurationProviderMock, "Test");
		myGraphitiCommand.toString();
		myGraphitiCommand = new GFCommand(configurationProviderMock);

		IContext contextMock = mock(IContext.class);
		myGraphitiCommand.setContext(contextMock);

		IContext context = myGraphitiCommand.getContext();
		assertEquals(contextMock, context);

		IFeature featureMock = mock(IFeature.class);
		myGraphitiCommand.setFeature(featureMock);

		IFeature feature = myGraphitiCommand.getFeature();
		assertEquals(featureMock, feature);

		// test
		ICreateFeature createFeatureMock = mock(ICreateFeature.class);

		ICreateContext createContextMock = mock(ICreateContext.class);

		CreateModelObjectCommand myCreateModelObjectCommand = new CreateModelObjectCommand(configurationProviderMock,
				createFeatureMock, createContextMock);

		myCreateModelObjectCommand.canExecute();

		myCreateModelObjectCommand.canUndo();

		myCreateModelObjectCommand.undo();

		// myCreateModelObjectCommand.execute();
	}

	@Test
	public void testUiInternalUtil() throws Exception {
		DataTypeTransformation myDataTypeTransformation = new DataTypeTransformation();
		assertNotNull(myDataTypeTransformation);

		org.eclipse.graphiti.mm.algorithms.styles.Point pointMock = mock(
				org.eclipse.graphiti.mm.algorithms.styles.Point.class);

		Point draw2dPoint = DataTypeTransformation.toDraw2dPoint(pointMock);
		assertNotNull(draw2dPoint);

		Bendpoint draw2dBendPoint = DataTypeTransformation.toDraw2dBendPoint(pointMock);
		assertNotNull(draw2dBendPoint);

		Vector<org.eclipse.graphiti.mm.algorithms.styles.Point> points = new Vector<org.eclipse.graphiti.mm.algorithms.styles.Point>();
		points.add(pointMock);
		points.add(pointMock);

		DataTypeTransformation.toDraw2dPointList(points);

		Color colorMock = mock(Color.class);

		EList<Color> colors = new BasicEList<Color>();
		colors.add(colorMock);

		Diagram diagramMock = mock(Diagram.class);
		when(diagramMock.getColors()).thenReturn(colors);

		assertTrue(DataTypeTransformation.toDraw2dLineStyle(LineStyle.DASH) == Graphics.LINE_DASH);
		assertTrue(DataTypeTransformation.toDraw2dLineStyle(LineStyle.DASHDOT) == Graphics.LINE_DASHDOT);
		assertTrue(DataTypeTransformation.toDraw2dLineStyle(LineStyle.DASHDOTDOT) == Graphics.LINE_DASHDOTDOT);
		assertTrue(DataTypeTransformation.toDraw2dLineStyle(LineStyle.DOT) == Graphics.LINE_DOT);
		assertTrue(DataTypeTransformation.toDraw2dLineStyle(LineStyle.SOLID) == Graphics.LINE_SOLID);
		assertTrue(DataTypeTransformation.toDraw2dLineStyle(null) == Graphics.LINE_SOLID);

		Font fontMock = mock(Font.class);
		when(fontMock.isItalic()).thenReturn(true);
		when(fontMock.isBold()).thenReturn(true);
		when(fontMock.getSize()).thenReturn(10);
		when(fontMock.getName()).thenReturn("Arial");

		FontData fontData = DataTypeTransformation.toFontData(fontMock);
		assertNotNull(fontData);
		DataTypeTransformation.toFontData(null);
	}

	@SuppressWarnings("restriction")
	@Test
	public void testFeaturesContextImpl() throws Exception {
		String s = null;

		org.eclipse.graphiti.mm.pictograms.Connection connectionMock = mock(
				org.eclipse.graphiti.mm.pictograms.Connection.class);

		Shape shapeMock = mock(Shape.class);

		SplitConnectionContext mySplitConnectionContext = new SplitConnectionContext(connectionMock, shapeMock);

		org.eclipse.graphiti.mm.pictograms.Connection con = mySplitConnectionContext.getConnection();
		assertNotNull(con);
		assertEquals(connectionMock, con);

		Shape shape = mySplitConnectionContext.getShape();
		assertNotNull(shape);
		assertEquals(shapeMock, shape);

		s = mySplitConnectionContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test AreaAnchorContext
		Anchor anchorMock = mock(Anchor.class);

		AreaAnchorContext myAreaAnchorContext = new AreaAnchorContext(anchorMock);

		Anchor anchor = myAreaAnchorContext.getAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		AnchorContainer containerMock = mock(AnchorContainer.class);

		myAreaAnchorContext.setSourceContainer(containerMock);
		AnchorContainer sourceContainer = myAreaAnchorContext.getSourceContainer();
		assertNotNull(sourceContainer);
		assertEquals(containerMock, sourceContainer);

		myAreaAnchorContext.setTargetContainer(containerMock);
		AnchorContainer targetContainer = myAreaAnchorContext.getTargetContainer();
		assertNotNull(targetContainer);
		assertEquals(containerMock, targetContainer);

		s = null;
		s = myAreaAnchorContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test CreateConnectionContext
		CreateConnectionContext myCreateConnectionContext = new CreateConnectionContext();

		myCreateConnectionContext.setSourceAnchor(anchorMock);
		anchor = myCreateConnectionContext.getSourceAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		myCreateConnectionContext.setTargetAnchor(anchorMock);
		anchor = myCreateConnectionContext.getTargetAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		PictogramElement peMock = mock(PictogramElement.class);

		myCreateConnectionContext.setSourcePictogramElement(peMock);
		PictogramElement pictogramElement = myCreateConnectionContext.getSourcePictogramElement();
		assertNotNull(pictogramElement);
		assertEquals(peMock, pictogramElement);

		myCreateConnectionContext.setTargetPictogramElement(peMock);
		pictogramElement = myCreateConnectionContext.getTargetPictogramElement();
		assertNotNull(pictogramElement);
		assertEquals(peMock, pictogramElement);

		s = null;
		s = myCreateConnectionContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test AddBendpointContext
		FreeFormConnection freeFormConnectionMock = mock(FreeFormConnection.class);

		AddBendpointContext myAddBendpointContext = new AddBendpointContext(freeFormConnectionMock, 0, 0, 0);

		myAddBendpointContext.getBendpoint();

		assertEquals(0, myAddBendpointContext.getBendpointIndex());

		FreeFormConnection freeFormConnection = myAddBendpointContext.getConnection();
		assertNotNull(freeFormConnection);
		assertEquals(freeFormConnectionMock, freeFormConnection);

		s = null;
		s = myAddBendpointContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test MoveConnectionDecoratorContext
		ConnectionDecorator connectionDecoratorMock = mock(ConnectionDecorator.class);

		MoveConnectionDecoratorContext myMoveConnectionDecoratorContext = new MoveConnectionDecoratorContext(connectionDecoratorMock, 0, 0,
				true);

		ConnectionDecorator connectionDecorator = myMoveConnectionDecoratorContext.getConnectionDecorator();
		assertNotNull(connectionDecorator);
		assertEquals(connectionDecoratorMock, connectionDecorator);

		assertTrue(myMoveConnectionDecoratorContext.isExecuteAllowed());

		s = null;
		s = myMoveConnectionDecoratorContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test ReconnectionContext
		ReconnectionContext myReconnectionContext = new ReconnectionContext(connectionMock, anchorMock, anchorMock, null);

		con = myReconnectionContext.getConnection();
		assertNotNull(con);
		assertEquals(connectionMock, con);

		anchor = myReconnectionContext.getNewAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		anchor = myReconnectionContext.getOldAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		myReconnectionContext.setTargetPictogramElement(peMock);

		pictogramElement = myReconnectionContext.getTargetPictogramElement();
		assertNotNull(pictogramElement);
		assertEquals(peMock, pictogramElement);

		s = null;
		s = myReconnectionContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		myReconnectionContext.setReconnectType("setReconnectType");
		assertEquals("setReconnectType", myReconnectionContext.getReconnectType());

		// test RemoveBendpointContext
		org.eclipse.graphiti.mm.algorithms.styles.Point pointMock = mock(
				org.eclipse.graphiti.mm.algorithms.styles.Point.class);

		RemoveBendpointContext myRemoveBendpointContext = new RemoveBendpointContext(freeFormConnectionMock, pointMock);

		myRemoveBendpointContext.setBendpointIndex(0);

		assertEquals(0, myRemoveBendpointContext.getBendpointIndex());

		freeFormConnection = myRemoveBendpointContext.getConnection();
		assertNotNull(freeFormConnection);
		assertEquals(freeFormConnectionMock, freeFormConnection);

		org.eclipse.graphiti.mm.algorithms.styles.Point bendpoint = myRemoveBendpointContext.getBendpoint();
		assertNotNull(bendpoint);
		assertEquals(pointMock, bendpoint);

		s = null;
		s = myRemoveBendpointContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test MoveContext
		MoveContext myMoveContext = new MoveContext();
		assertNotNull(myMoveContext);
		myMoveContext = new MoveContext(0, 0);
		assertNotNull(myMoveContext);

		// test MoveBendpointContext
		MoveBendpointContext myMoveBendpointContext = new MoveBendpointContext(pointMock);

		bendpoint = myMoveBendpointContext.getBendpoint();
		assertNotNull(bendpoint);
		assertEquals(pointMock, bendpoint);

		myMoveBendpointContext.setBendpointIndex(0);
		assertEquals(0, myMoveBendpointContext.getBendpointIndex());

		myMoveBendpointContext.setConnection(freeFormConnectionMock);
		freeFormConnection = myMoveBendpointContext.getConnection();
		assertNotNull(freeFormConnection);
		assertEquals(freeFormConnectionMock, freeFormConnection);

		s = null;
		s = myMoveBendpointContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test SaveImageContext
		SaveImageContext mySaveImageContext = new SaveImageContext();

		mySaveImageContext = new SaveImageContext();

		s = null;
		s = mySaveImageContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test LocationContext
		LocationContext myLocationContext = new LocationContext();

		myLocationContext = new LocationContext(10, 10);
		myLocationContext.setX(10);
		myLocationContext.setY(10);
		assertEquals(10, myLocationContext.getX());
		assertEquals(10, myLocationContext.getY());

		s = null;
		s = myLocationContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// AreaContext
		AreaContext myAreaContext = new AreaContext();

		myAreaContext.setWidth(10);
		assertEquals(10, myAreaContext.getWidth());

		myAreaContext.setHeight(10);
		assertEquals(10, myAreaContext.getHeight());

		myAreaContext.setSize(20, 20);
		assertEquals(20, myAreaContext.getWidth());
		assertEquals(20, myAreaContext.getHeight());

		s = myAreaContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test ResizeShapeContext
		ResizeShapeContext myResizeShapeContext = new ResizeShapeContext(shapeMock);

		shape = myResizeShapeContext.getShape();
		assertNotNull(shape);
		assertEquals(shapeMock, shape);

		pictogramElement = myResizeShapeContext.getPictogramElement();
		assertNotNull(pictogramElement);
		assertEquals(shapeMock, pictogramElement);

		s = null;
		s = myResizeShapeContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test ResizeContext
		ResizeContext myResizeContext = new ResizeContext();

		myResizeContext.setHeight(10);
		assertEquals(10, myResizeContext.getHeight());

		myResizeContext.setWidth(15);
		assertEquals(15, myResizeContext.getWidth());

		myResizeContext.setSize(20, 20);
		assertEquals(20, myResizeContext.getHeight());
		assertEquals(20, myResizeContext.getWidth());

		s = null;
		s = myResizeContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test PrintContext
		PrintContext myPrintContext = new PrintContext();
		assertNotNull(myPrintContext);

		// test MoveShapeContext
		MoveShapeContext myMoveShapeContext = new MoveShapeContext(shapeMock);

		shape = myMoveShapeContext.getShape();
		assertNotNull(shape);
		assertEquals(shapeMock, shape);

		myMoveShapeContext.setDeltaX(10);
		assertEquals(10, myMoveShapeContext.getDeltaX());

		myMoveShapeContext.setDeltaY(15);
		assertEquals(15, myMoveShapeContext.getDeltaY());

		ContainerShape containerShapeMock = mock(ContainerShape.class);

		myMoveShapeContext.setSourceContainer(containerShapeMock);
		ContainerShape containerShape = myMoveShapeContext.getSourceContainer();
		assertNotNull(containerShape);
		assertEquals(containerShapeMock, containerShape);

		myMoveShapeContext.setTargetContainer(containerShapeMock);
		containerShape = myMoveShapeContext.getTargetContainer();
		assertNotNull(containerShape);
		assertEquals(containerShapeMock, containerShape);

		myMoveShapeContext.setTargetConnection(connectionMock);
		con = myMoveShapeContext.getTargetConnection();
		assertNotNull(con);
		assertEquals(connectionMock, con);

		pictogramElement = myMoveShapeContext.getPictogramElement();
		assertNotNull(pictogramElement);
		assertEquals(shapeMock, pictogramElement);

		s = null;
		s = myMoveShapeContext.toString();
		assertNotNull(s);
		assertTrue(!("".equals(s)));

		// test LayoutContext
		LayoutContext myLayoutContext = new LayoutContext(peMock);
		assertNotNull(myLayoutContext);

		// test DirectEditingContext
		GraphicsAlgorithm graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);

		DirectEditingContext myDirectEditingContext = new DirectEditingContext(peMock, graphicsAlgorithmMock);
		assertNotNull(myDirectEditingContext);

		// test AddConnectionContext
		AddConnectionContext myAddConnectionContext = new AddConnectionContext(anchorMock, anchorMock);

		anchor = myAddConnectionContext.getSourceAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		anchor = myAddConnectionContext.getTargetAnchor();
		assertNotNull(anchor);
		assertEquals(anchorMock, anchor);

		// test CustomContext
		PictogramElement[] pictogramElements1 = new PictogramElement[] { peMock };
		CustomContext myCustomContext = new CustomContext(pictogramElements1);

		PictogramElement[] pictogramElements2 = myCustomContext.getPictogramElements();
		assertNotNull(pictogramElements2);
		assertArrayEquals(pictogramElements1, pictogramElements2);

		myCustomContext.setInnerPictogramElement(peMock);
		pictogramElement = myCustomContext.getInnerPictogramElement();
		assertNotNull(pictogramElement);
		assertEquals(peMock, pictogramElement);

		myCustomContext.setInnerGraphicsAlgorithm(graphicsAlgorithmMock);
		GraphicsAlgorithm graphicsAlgorithm = myCustomContext.getInnerGraphicsAlgorithm();
		assertNotNull(graphicsAlgorithm);
		assertEquals(graphicsAlgorithmMock, graphicsAlgorithm);

		// test AddContext
		IAreaContext areaContextMock = mock(IAreaContext.class);

		Object value = new Object();
		AddContext myAddContext = new AddContext(areaContextMock, value);

		myAddContext.setTargetConnection(connectionMock);
		con = myAddContext.getTargetConnection();
		assertNotNull(con);
		assertEquals(connectionMock, con);

		myAddContext.setTargetContainer(containerShapeMock);
		containerShape = myAddContext.getTargetContainer();
		assertNotNull(containerShape);
		assertEquals(containerShapeMock, containerShape);

		// test CreateContext
		CreateContext myCreateContext = new CreateContext();

		myCreateContext.setTargetConnection(connectionMock);
		con = myCreateContext.getTargetConnection();
		assertNotNull(con);
		assertEquals(connectionMock, con);

		myCreateContext.setTargetContainer(containerShapeMock);
		containerShape = myCreateContext.getTargetContainer();
		assertNotNull(containerShape);
		assertEquals(containerShapeMock, containerShape);
	}

	@Test
	public void testInternal() throws Exception {
		// test constructor
		ExternalPictogramLink myExternalPictogramLink = new ExternalPictogramLink();

		try {
			List<EObject> list = myExternalPictogramLink.getBusinessObjects();
			assertNotNull(list);
		} catch (UnsupportedOperationException e) {
			// do nothing
		}

		// test stub
		EObject refObjectMock = mock(EObject.class);
		// myExternalPictogramLink.refSetValue(refObjectMock, new Object());
		// myExternalPictogramLink.refInvokeOperation(new String(), new
		// Vector<Object>());
		// // assertNotNull(operation);
		// myExternalPictogramLink.refInvokeOperation(refObjectMock, new
		// Vector<Object>());
		// // assertNotNull(operation2);
		// myExternalPictogramLink.refGetValue(new String());
		// // assertNotNull(value);
		// myExternalPictogramLink.refGetValue(refObjectMock);
		// // assertNotNull(value2);
		// myExternalPictogramLink.refOutermostComposite();
		// // assertNotNull(refOutermostComposite);
		// boolean isInstanceOf =
		// myExternalPictogramLink.erefIsInstanceOf(refObjectMock, true);
		// isInstanceOf = !(isInstanceOf);
		// boolean isInstanceOf2 =
		// myExternalPictogramLink.refIsInstanceOf(refObjectMock, false);
		// isInstanceOf2 = !(isInstanceOf2);
		// myExternalPictogramLink.refImmediateComposite();
		// // assertNotNull(refImmediateComposite)
		// myExternalPictogramLink.refDelete();
		// myExternalPictogramLink.refClass();
		// // assertNotNull(refClass);
		// myExternalPictogramLink.setWidth(10);
		// myExternalPictogramLink.getWidth();
		// myExternalPictogramLink.setHeight(10);
		// myExternalPictogramLink.getHeight();
		myExternalPictogramLink.getProperties();
		// assertNotNull(properties);
		// end of test stub

		assertNull(myExternalPictogramLink.getPictogramElement());

		PictogramElement pictogramElementMock = mock(PictogramElement.class);
		myExternalPictogramLink.setPictogramElement(pictogramElementMock);

		PictogramElement pictogramElement = myExternalPictogramLink.getPictogramElement();
		assertNotNull(pictogramElement);
		assertTrue(pictogramElement.equals(pictogramElementMock));

		// DiagramLink was removed
		/*
		 * DiagramLink diagramLinkMock = mock(DiagramLink.class);
		 * myExternalPictogramLink.setDiagramLink(diagramLinkMock);
		 * 
		 * DiagramLink diagramLink = myExternalPictogramLink.getDiagramLink();
		 * assertNotNull(diagramLink);
		 * assertTrue(diagramLink.equals(diagramLinkMock));
		 */

		// ####################################################
		IGaService gaService = Graphiti.getGaService();

		Style styleMock;
		AbstractText abstractTextMock;

		// test public static Color getBackgroundColor(GraphicsAlgorithm ga,
		// boolean checkStyles)
		// and private static Color getBackgroundColor(Style style)
		Color colorMock = mock(Color.class);

		styleMock = mock(Style.class);
		when(styleMock.getBackground()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getBackground()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getBackground()).thenReturn(colorMock);

		GraphicsAlgorithm graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.getBackground()).thenReturn(colorMock);
		// run 2
		when(graphicsAlgorithmMock.getBackground()).thenReturn(null);
		// run 3
		when(graphicsAlgorithmMock.getBackground()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.getBackground()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.getBackgroundColor(graphicsAlgorithmMock, false);
		gaService.getBackgroundColor(graphicsAlgorithmMock, false);
		gaService.getBackgroundColor(graphicsAlgorithmMock, true);
		gaService.getBackgroundColor(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static Font getFont(AbstractText at, boolean checkStyles)
		// and private static Font getFont(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getFont()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getFont()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getFont()).thenReturn(mock(Font.class));
		when(styleMock.getFont()).thenReturn(mock(Font.class));

		abstractTextMock = mock(AbstractText.class);
		// run 1
		when(abstractTextMock.getFont()).thenReturn(null);
		// run 2
		when(abstractTextMock.getFont()).thenReturn(mock(Font.class));
		when(abstractTextMock.getFont()).thenReturn(null);
		// run 3
		when(abstractTextMock.getFont()).thenReturn(null);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(abstractTextMock.getFont()).thenReturn(null);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		gaService.getFont(abstractTextMock, false);
		gaService.getFont(abstractTextMock, true);
		gaService.getFont(abstractTextMock, true);
		gaService.getFont(abstractTextMock, true);

		// verify(styleMock);

		// test public static Color getForegroundColor(GraphicsAlgorithm ga,
		// boolean checkStyles)
		// and private static Color getForegroundColor(Style style)
		colorMock = mock(Color.class);

		styleMock = mock(Style.class);
		when(styleMock.getForeground()).thenReturn(null, null, colorMock, colorMock);
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.getForeground()).thenReturn(colorMock);
		// run 2
		when(graphicsAlgorithmMock.getForeground()).thenReturn(null);
		// run 3
		when(graphicsAlgorithmMock.getForeground()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.getForeground()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.getForegroundColor(graphicsAlgorithmMock, false);
		gaService.getForegroundColor(graphicsAlgorithmMock, false);
		gaService.getForegroundColor(graphicsAlgorithmMock, true);
		gaService.getForegroundColor(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static Orientation getHorizontalAlignment(AbstractText
		// at, boolean checkStyles)
		// and private static Orientation getHorizontalAlignment(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getHorizontalAlignment()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getHorizontalAlignment()).thenReturn(Orientation.ALIGNMENT_RIGHT);
		when(styleMock.getStyleContainer()).thenReturn(null);

		abstractTextMock = mock(AbstractText.class);
		// run 1
		when(abstractTextMock.getHorizontalAlignment()).thenReturn(Orientation.UNSPECIFIED);
		// run 2
		when(abstractTextMock.getHorizontalAlignment()).thenReturn(Orientation.ALIGNMENT_LEFT);
		// run 3
		when(abstractTextMock.getHorizontalAlignment()).thenReturn(Orientation.UNSPECIFIED);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(abstractTextMock.getHorizontalAlignment()).thenReturn(Orientation.UNSPECIFIED);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		gaService.getHorizontalAlignment(abstractTextMock, false);
		gaService.getHorizontalAlignment(abstractTextMock, false);
		gaService.getHorizontalAlignment(abstractTextMock, true);
		gaService.getHorizontalAlignment(abstractTextMock, true);

		// verify(styleMock);

		// test public static Orientation getVerticalAlignment(AbstractText at,
		// boolean checkStyles)
		// and private static Orientation getVerticalAlignment(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getVerticalAlignment()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getVerticalAlignment()).thenReturn(Orientation.ALIGNMENT_TOP);
		when(styleMock.getStyleContainer()).thenReturn(null);

		abstractTextMock = mock(AbstractText.class);
		// run 1
		when(abstractTextMock.getVerticalAlignment()).thenReturn(Orientation.UNSPECIFIED);
		// run 2
		when(abstractTextMock.getVerticalAlignment()).thenReturn(Orientation.ALIGNMENT_BOTTOM);
		// run 3
		when(abstractTextMock.getVerticalAlignment()).thenReturn(Orientation.UNSPECIFIED);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(abstractTextMock.getVerticalAlignment()).thenReturn(Orientation.UNSPECIFIED);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		gaService.getVerticalAlignment(abstractTextMock, false);
		gaService.getVerticalAlignment(abstractTextMock, false);
		gaService.getVerticalAlignment(abstractTextMock, true);
		gaService.getVerticalAlignment(abstractTextMock, true);

		// verify(styleMock);

		// test public static LineStyle getLineStyle(GraphicsAlgorithm ga,
		// boolean checkStyles)
		// and private static LineStyle getLineStyle(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getLineStyle()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getLineStyle()).thenReturn(LineStyle.DASHDOT);
		when(styleMock.getStyleContainer()).thenReturn(null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.getLineStyle()).thenReturn(LineStyle.UNSPECIFIED);
		// run 2
		when(graphicsAlgorithmMock.getLineStyle()).thenReturn(LineStyle.SOLID);
		// run 3
		when(graphicsAlgorithmMock.getLineStyle()).thenReturn(LineStyle.UNSPECIFIED);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.getLineStyle()).thenReturn(LineStyle.UNSPECIFIED);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.getLineStyle(graphicsAlgorithmMock, false);
		gaService.getLineStyle(graphicsAlgorithmMock, false);
		gaService.getLineStyle(graphicsAlgorithmMock, true);
		gaService.getLineStyle(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static int getLineWidth(GraphicsAlgorithm ga, boolean
		// checkStyles) {
		// and private static int getLineWidth(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getLineWidth()).thenReturn(null, null, Integer.valueOf(5), Integer.valueOf(5));
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.getLineWidth()).thenReturn(null);
		// run 2
		when(graphicsAlgorithmMock.getLineWidth()).thenReturn(Integer.valueOf(6));
		// run 3
		when(graphicsAlgorithmMock.getLineWidth()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.getLineWidth()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.getLineWidth(graphicsAlgorithmMock, false);
		gaService.getLineWidth(graphicsAlgorithmMock, false);
		gaService.getLineWidth(graphicsAlgorithmMock, true);
		gaService.getLineWidth(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static RenderingStyle getRenderingStyle(GraphicsAlgorithm
		// ga, boolean checkStyles)
		// and private static RenderingStyle getRenderingStyle(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getRenderingStyle()).thenReturn(null, null, mock(RenderingStyle.class),
				mock(RenderingStyle.class));
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.getRenderingStyle()).thenReturn(null);
		// run 2
		when(graphicsAlgorithmMock.getRenderingStyle()).thenReturn(mock(RenderingStyle.class));
		// run 3
		when(graphicsAlgorithmMock.getRenderingStyle()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.getRenderingStyle()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.getRenderingStyle(graphicsAlgorithmMock, false);
		gaService.getRenderingStyle(graphicsAlgorithmMock, false);
		gaService.getRenderingStyle(graphicsAlgorithmMock, true);
		gaService.getRenderingStyle(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static double getTransparency(GraphicsAlgorithm ga,
		// boolean checkStyles)
		// and private static double getTransparency(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getTransparency()).thenReturn(null, null, Double.valueOf(1.0), Double.valueOf(1.0));
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.getTransparency()).thenReturn(null);
		// run 2
		when(graphicsAlgorithmMock.getTransparency()).thenReturn(Double.valueOf(0.0));
		// run 3
		when(graphicsAlgorithmMock.getTransparency()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.getTransparency()).thenReturn(null);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.getTransparency(graphicsAlgorithmMock, false);
		gaService.getTransparency(graphicsAlgorithmMock, false);
		gaService.getTransparency(graphicsAlgorithmMock, true);
		gaService.getTransparency(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static boolean isFilled(GraphicsAlgorithm ga, boolean
		// checkStyles)
		// and private static boolean isFilled(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getFilled()).thenReturn(null, null, Boolean.valueOf(true), Boolean.valueOf(true));
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.isSetFilled()).thenReturn(false);
		// run 2
		when(graphicsAlgorithmMock.isSetFilled()).thenReturn(true);
		when(graphicsAlgorithmMock.getFilled()).thenReturn(Boolean.valueOf(false));
		// run 3
		when(graphicsAlgorithmMock.isSetFilled()).thenReturn(false);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.isSetFilled()).thenReturn(false);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.isFilled(graphicsAlgorithmMock, false);
		gaService.isFilled(graphicsAlgorithmMock, false);
		gaService.isFilled(graphicsAlgorithmMock, true);
		gaService.isFilled(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static boolean isLineVisible(GraphicsAlgorithm ga,
		// boolean checkStyles)
		// and private static boolean isLineVisible(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getLineVisible()).thenReturn(null, null, Boolean.valueOf(true), Boolean.valueOf(true));
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		// run 1
		when(graphicsAlgorithmMock.isSetLineVisible()).thenReturn(false);
		// run 2
		when(graphicsAlgorithmMock.isSetLineVisible()).thenReturn(true);
		when(graphicsAlgorithmMock.getLineVisible()).thenReturn(Boolean.valueOf(false));
		// run 3
		when(graphicsAlgorithmMock.isSetLineVisible()).thenReturn(false);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(graphicsAlgorithmMock.isSetLineVisible()).thenReturn(false);
		when(graphicsAlgorithmMock.getStyle()).thenReturn(styleMock);

		gaService.isLineVisible(graphicsAlgorithmMock, false);
		gaService.isLineVisible(graphicsAlgorithmMock, false);
		gaService.isLineVisible(graphicsAlgorithmMock, true);
		gaService.isLineVisible(graphicsAlgorithmMock, true);

		// verify(styleMock);

		// test public static boolean isProportional(Image image, boolean
		// checkStyles)
		// and private static boolean isProportional(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getProportional()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getProportional()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getProportional()).thenReturn(Boolean.valueOf(true));

		Image imageMock = mock(Image.class);
		// run 1
		when(imageMock.getProportional()).thenReturn(null);
		// run 2
		when(imageMock.getProportional()).thenReturn(Boolean.valueOf(false));
		// run 3
		when(imageMock.getProportional()).thenReturn(null);
		when(imageMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(imageMock.getProportional()).thenReturn(null);
		when(imageMock.getStyle()).thenReturn(styleMock);

		gaService.isProportional(imageMock, false);
		gaService.isProportional(imageMock, false);
		gaService.isProportional(imageMock, true);
		gaService.isProportional(imageMock, true);

		// verify(styleMock);

		// test public static boolean isStretchH(Image image, boolean
		// checkStyles)
		// and private static boolean isStretchH(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getStretchH()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getStretchH()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getStretchH()).thenReturn(Boolean.valueOf(true));

		imageMock = mock(Image.class);
		// run 1
		when(imageMock.getStretchH()).thenReturn(null);
		// run 2
		when(imageMock.getStretchH()).thenReturn(Boolean.valueOf(false));
		// run 3
		when(imageMock.getStretchH()).thenReturn(null);
		when(imageMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(imageMock.getStretchH()).thenReturn(null);
		when(imageMock.getStyle()).thenReturn(styleMock);

		gaService.isStretchH(imageMock, false);
		gaService.isStretchH(imageMock, false);
		gaService.isStretchH(imageMock, true);
		gaService.isStretchH(imageMock, true);

		// verify(styleMock);

		// test public static boolean isStretchV(Image image, boolean
		// checkStyles)
		// and private static boolean isStretchV(Style style)
		styleMock = mock(Style.class);
		when(styleMock.getStretchV()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getStretchV()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getStretchV()).thenReturn(Boolean.valueOf(true));

		imageMock = mock(Image.class);
		// run 1
		when(imageMock.getStretchV()).thenReturn(null);
		// run 2
		when(imageMock.getStretchV()).thenReturn(Boolean.valueOf(false));
		// run 3
		when(imageMock.getStretchV()).thenReturn(null);
		when(imageMock.getStyle()).thenReturn(styleMock);
		// run 4
		when(imageMock.getStretchV()).thenReturn(null);
		when(imageMock.getStyle()).thenReturn(styleMock);

		gaService.isStretchV(imageMock, false);
		gaService.isStretchV(imageMock, false);
		gaService.isStretchV(imageMock, true);
		gaService.isStretchV(imageMock, true);

		// verify(styleMock);
	}

	@Test
	public void testRotationAngleCombination1() {
		IGaService gaService = Graphiti.getGaService();
		Style styleContainerMock = mock(Style.class);
		when(styleContainerMock.getRotation()).thenReturn(null);
		when(styleContainerMock.getStyleContainer()).thenReturn(null);

		Style styleMock = mock(Style.class);
		when(styleMock.getRotation()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);

		AbstractText abstractTextMock = mock(AbstractText.class);
		when(abstractTextMock.getAngle()).thenReturn(null);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		assertEquals(0, gaService.getRotation(abstractTextMock, false), 0);
	}

	@Test
	public void testRotationAngleCombination2() {
		IGaService gaService = Graphiti.getGaService();
		Style styleContainerMock = mock(Style.class);
		when(styleContainerMock.getRotation()).thenReturn(null);
		when(styleContainerMock.getStyleContainer()).thenReturn(null);

		Style styleMock = mock(Style.class);
		when(styleMock.getStyleContainer()).thenReturn(styleMock);
		when(styleMock.getRotation()).thenReturn(null);
		when(styleMock.getStyleContainer()).thenReturn(null);

		AbstractText abstractTextMock = mock(AbstractText.class);
		when(abstractTextMock.getAngle()).thenReturn(Integer.valueOf(0));
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		assertEquals(0, gaService.getRotation(abstractTextMock, false), 0);
	}

	@Test
	public void testRotationAngleCombination3() {
		IGaService gaService = Graphiti.getGaService();
		Style styleContainerMock = mock(Style.class);
		when(styleContainerMock.getRotation()).thenReturn(null);
		when(styleContainerMock.getStyleContainer()).thenReturn(null);

		Style styleMock = mock(Style.class);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getRotation()).thenReturn(Double.valueOf(-1));

		AbstractText abstractTextMock = mock(AbstractText.class);
		when(abstractTextMock.getAngle()).thenReturn(null);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		assertEquals(0, gaService.getRotation(abstractTextMock, true), 0);
	}

	@Test
	public void testRotationAngleCombination4() {
		IGaService gaService = Graphiti.getGaService();
		Style styleContainerMock = mock(Style.class);
		when(styleContainerMock.getRotation()).thenReturn(null);
		when(styleContainerMock.getStyleContainer()).thenReturn(null);

		Style styleMock = mock(Style.class);
		when(styleMock.getStyleContainer()).thenReturn(null);
		when(styleMock.getRotation()).thenReturn(Double.valueOf(-1));

		AbstractText abstractTextMock = mock(AbstractText.class);
		when(abstractTextMock.getRotation()).thenReturn(null);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		assertEquals(-1, gaService.getRotation(abstractTextMock, true), 0);
	}

	@Test
	public void testPaletteImpl() throws Exception {
		StackEntry stackEntry = new StackEntry("label", "description", null);
		stackEntry.addCreationToolEntry(null);
		assertNotNull(stackEntry.getCreationToolEntries());
		assertEquals("description", stackEntry.getDescription());
	}

	@Test
	public void testPlatformGa() throws Exception {

		PlatformGraphicsAlgorithm platformGraphicsAlgorithmMock = mock(PlatformGraphicsAlgorithm.class);

		IDiagramTypeProvider diagramTypeProviderMock = mock(IDiagramTypeProvider.class);
		IFeatureProvider featureProviderMock = mock(IFeatureProvider.class);
		when(diagramTypeProviderMock.getFeatureProvider()).thenReturn(featureProviderMock);

		// test constructor
		RendererContext myRendererContext = new RendererContext(platformGraphicsAlgorithmMock, diagramTypeProviderMock);

		// test getDiagramTypeProvider()
		IDiagramTypeProvider diagramTypeProvider = myRendererContext.getDiagramTypeProvider();
		assertNotNull(diagramTypeProvider);
		assertTrue(diagramTypeProvider.equals(diagramTypeProviderMock));

		// test getGraphicsAlgorithm()
		GraphicsAlgorithm graphicsAlgorithm = myRendererContext.getGraphicsAlgorithm();
		assertNotNull(graphicsAlgorithm);

		// test getMappingProvider()
		IMappingProvider mappingProvider = myRendererContext.getMappingProvider();
		assertNotNull(mappingProvider);

		// test getPlatformGraphicsAlgorithm()
		PlatformGraphicsAlgorithm platformGraphicsAlgorithm = myRendererContext.getPlatformGraphicsAlgorithm();
		assertNotNull(platformGraphicsAlgorithm);
		assertTrue(platformGraphicsAlgorithm.equals(platformGraphicsAlgorithmMock));
	}

	@Test
	public void testDiagramEditorFactory() {
		TransactionalEditingDomain ted = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		assertNotNull(ted);
		ResourceSet rSet = ted.getResourceSet();
		TransactionalEditingDomain ted2 = TransactionUtil.getEditingDomain(rSet);
		assertEquals(ted, ted2);

	}

	@Test
	public void testGaServiceGetRotation() throws Exception {
		Style styleMock = mock(Style.class);
		when(styleMock.getRotation()).thenReturn(null, null, Double.valueOf(-1));
		when(styleMock.getStyleContainer()).thenReturn(styleMock, (StyleContainer) null);

		AbstractText abstractTextMock = mock(AbstractText.class);
		when(abstractTextMock.getRotation()).thenReturn(null, Double.valueOf(0), null, null);
		when(abstractTextMock.getStyle()).thenReturn(styleMock);

		IGaService gaService = Graphiti.getGaService();

		assertEquals(0, gaService.getRotation(abstractTextMock, false), 0d);
		assertEquals(0, gaService.getRotation(abstractTextMock, false), 0d);
		assertEquals(0, gaService.getRotation(abstractTextMock, true), 0d);
		assertEquals(-1, gaService.getRotation(abstractTextMock, true), 0d);
	}
}
