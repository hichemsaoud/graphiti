/*********************************************************************
* Copyright (c) 2014, 2022 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    mwenz - Bug 443304 - Improve undo/redo handling in Graphiti features
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.ICustomAbortableUndoRedoFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.IReconnectionContext;
import org.eclipse.graphiti.features.context.impl.ReconnectionContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.features.impl.DefaultReconnectionFeature;
import org.eclipse.graphiti.internal.command.CommandContainer;
import org.eclipse.graphiti.internal.command.GenericFeatureCommandWithContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;
import org.eclipse.graphiti.tests.reuse.GFAbstractTestCase;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.editor.IDiagramContainerUI;
import org.eclipse.graphiti.ui.internal.command.GefCommandWrapper;
import org.eclipse.graphiti.ui.internal.command.ReconnectCommand;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderInternal;
import org.eclipse.graphiti.ui.internal.editor.GFCommandStack;
import org.eclipse.graphiti.ui.platform.IConfigurationProvider;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.junit.Test;

/**
 *
 */
@SuppressWarnings("restriction")
public class CustomUndoRedoFeatureTest extends GFAbstractTestCase {

	@Test
	public void testPositive() {
		TransactionalEditingDomain editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		IToolBehaviorProvider toolBehaviorProvider = mock(IToolBehaviorProvider.class);

		IDiagramTypeProvider diagramTypeProvider = mock(IDiagramTypeProvider.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);

		when(diagramTypeProvider.getCurrentToolBehaviorProvider()).thenReturn(toolBehaviorProvider);

		IConfigurationProvider configurationProvider = mock(IConfigurationProviderInternal.class);
		when(configurationProvider.getDiagramTypeProvider()).thenReturn(diagramTypeProvider);
		GFCommandStack commandStack = new GFCommandStack(configurationProvider, editingDomain);

		ICustomContext context = mock(ICustomContext.class);

		// Do not abort after pre hook
		TestCustomFeature feature = new TestCustomFeature(featureProvider, false);
		GenericFeatureCommandWithContext featureCommand = new GenericFeatureCommandWithContext(feature, context);

		CommandContainer commandContainer = new CommandContainer(featureProvider);
		commandContainer.add(featureCommand);

		GefCommandWrapper commandWrapper = new GefCommandWrapper(commandContainer, editingDomain);
		commandStack.execute(commandWrapper);

		// Check that feature can be undone
		assertTrue("Executed command must be undoable", commandStack.canUndo());

		// Check that undo is called
		commandStack.undo();
		assertTrue("preUndo() must have been called", feature.preUndoCalled);
		assertTrue("postUndo() must have been called", feature.postUndoCalled);

		// Check that feature can be redone
		assertTrue("Executed command must be redoable", commandStack.canRedo());

		// Check that redo is called
		commandStack.redo();
		assertTrue("preRedo() must have been called", feature.preRedoCalled);
		assertTrue("postRedo() must have been called", feature.postRedoCalled);
	}

	@Test
	public void testAbort() {
		TransactionalEditingDomain editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		IToolBehaviorProvider toolBehaviorProvider = mock(IToolBehaviorProvider.class);

		IDiagramTypeProvider diagramTypeProvider = mock(IDiagramTypeProvider.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);

		when(diagramTypeProvider.getCurrentToolBehaviorProvider()).thenReturn(toolBehaviorProvider);

		IConfigurationProvider configurationProvider = mock(IConfigurationProviderInternal.class);
		when(configurationProvider.getDiagramTypeProvider()).thenReturn(diagramTypeProvider);
		GFCommandStack commandStack = new GFCommandStack(configurationProvider, editingDomain);

		ICustomContext context = mock(ICustomContext.class);

		// Do abort after pre hook
		TestCustomFeature feature = new TestCustomFeature(featureProvider, true);
		GenericFeatureCommandWithContext featureCommand = new GenericFeatureCommandWithContext(feature, context);

		CommandContainer commandContainer = new CommandContainer(featureProvider);
		commandContainer.add(featureCommand);

		GefCommandWrapper commandWrapper = new GefCommandWrapper(commandContainer, editingDomain);
		commandStack.execute(commandWrapper);

		// Check that feature can be undone
		assertTrue("Executed command must be undoable", commandStack.canUndo());

		// Check that undo is called
		try {
			commandStack.undo();
			fail("Abort expected");
		} catch (OperationCanceledException e) {
			// Expected
		}
		assertTrue("preUndo() must have been called", feature.preUndoCalled);
		assertFalse("postUndo() must not have been called", feature.postUndoCalled);
	}

	@Test
	public void testReconnect() {
		TransactionalEditingDomain editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		IToolBehaviorProvider toolBehaviorProvider = mock(IToolBehaviorProvider.class);

		IDiagramTypeProvider diagramTypeProvider = mock(IDiagramTypeProvider.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);
		// Do not abort after pre hook
		TestReconnectionFeature feature = new TestReconnectionFeature(featureProvider, false);
		when(featureProvider.getReconnectionFeature(any())).thenReturn(feature);

		when(diagramTypeProvider.getCurrentToolBehaviorProvider()).thenReturn(toolBehaviorProvider);
		when(diagramTypeProvider.getFeatureProvider()).thenReturn(featureProvider);

		IDiagramContainerUI diagramContainer = mock(IDiagramContainerUI.class);

		DiagramBehavior diagramBehavior = new MockDiagramBehavior(diagramContainer, editingDomain);

		IConfigurationProvider configurationProvider = mock(IConfigurationProviderInternal.class);
		when(configurationProvider.getDiagramTypeProvider()).thenReturn(diagramTypeProvider);
		when(configurationProvider.getDiagramBehavior()).thenReturn(diagramBehavior);
		GFCommandStack commandStack = new GFCommandStack(configurationProvider, editingDomain);

		Connection connection = mock(Connection.class);
		Anchor oldAnchor = mock(Anchor.class);
		Anchor newAnchor = mock(Anchor.class);
		PictogramElement newTargetPictogramElement = mock(PictogramElement.class);

		ReconnectCommand reconnectCommand = new ReconnectCommand(configurationProvider, connection, oldAnchor,
				newAnchor, newTargetPictogramElement, ReconnectionContext.RECONNECT_SOURCE, null);
		commandStack.execute(reconnectCommand);

		// Check that feature can be undone
		assertTrue("Executed command must be undoable", commandStack.canUndo());

		// Check that undo is called
		commandStack.undo();
		assertTrue("preUndo() must have been called", feature.preUndoCalled);
		assertTrue("postUndo() must have been called", feature.postUndoCalled);

		// Check that feature can be redone
		assertTrue("Executed command must be redoable", commandStack.canRedo());

		// Check that redo is called
		commandStack.redo();
		assertTrue("preRedo() must have been called", feature.preRedoCalled);
		assertTrue("postRedo() must have been called", feature.postRedoCalled);
	}

	@Test
	public void testReconnectAbort() {
		TransactionalEditingDomain editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		IToolBehaviorProvider toolBehaviorProvider = mock(IToolBehaviorProvider.class);

		IDiagramTypeProvider diagramTypeProvider = mock(IDiagramTypeProvider.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);
		// Do abort after pre hook
		TestReconnectionFeature feature = new TestReconnectionFeature(featureProvider, true);
		when(featureProvider.getReconnectionFeature(any())).thenReturn(feature);

		when(diagramTypeProvider.getCurrentToolBehaviorProvider()).thenReturn(toolBehaviorProvider);
		when(diagramTypeProvider.getFeatureProvider()).thenReturn(featureProvider);

		IDiagramContainerUI diagramContainer = mock(IDiagramContainerUI.class);

		DiagramBehavior diagramBehavior = new MockDiagramBehavior(diagramContainer, editingDomain);

		IConfigurationProvider configurationProvider = mock(IConfigurationProviderInternal.class);
		when(configurationProvider.getDiagramTypeProvider()).thenReturn(diagramTypeProvider);
		when(configurationProvider.getDiagramBehavior()).thenReturn(diagramBehavior);
		GFCommandStack commandStack = new GFCommandStack(configurationProvider, editingDomain);

		Connection connection = mock(Connection.class);
		Anchor oldAnchor = mock(Anchor.class);
		Anchor newAnchor = mock(Anchor.class);
		PictogramElement newTargetPictogramElement = mock(PictogramElement.class);

		ReconnectCommand reconnectCommand = new ReconnectCommand(configurationProvider, connection, oldAnchor,
				newAnchor, newTargetPictogramElement, ReconnectionContext.RECONNECT_SOURCE, null);
		commandStack.execute(reconnectCommand);

		// Check that feature can be undone
		assertTrue("Executed command must be undoable", commandStack.canUndo());

		// Check that undo is called
		try {
			commandStack.undo();
			fail("Abort expected");
		} catch (OperationCanceledException e) {
			// Expected
		}
		assertTrue("preUndo() must have been called", feature.preUndoCalled);
		assertFalse("postUndo() must not have been called", feature.postUndoCalled);
	}

	private class TestCustomFeature extends AbstractCustomFeature implements ICustomAbortableUndoRedoFeature {

		public boolean preUndoCalled = false;
		public boolean postUndoCalled = false;

		public boolean preRedoCalled = false;
		public boolean postRedoCalled = false;

		private ICustomContext context = null;
		private boolean abortAfterPreHook;

		public TestCustomFeature(IFeatureProvider fp, boolean abortAfterPreHook) {
			super(fp);
			this.abortAfterPreHook = abortAfterPreHook;
		}

		@Override
		public boolean canExecute(ICustomContext context) {
			return true;
		}

		@Override
		public boolean isAbort() {
			return abortAfterPreHook;
		}

		public void execute(ICustomContext context) {
			// Do nothing
			this.context = context;
		}

		@Override
		public boolean canUndo(IContext context) {
			return true;
		}

		@Override
		public void preUndo(IContext context) {
			preUndoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		@Override
		public void postUndo(IContext context) {
			postUndoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		public boolean canRedo(IContext context) {
			return true;
		}

		@Override
		public void preRedo(IContext context) {
			preRedoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		@Override
		public void postRedo(IContext context) {
			postRedoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}
	}

	private class TestReconnectionFeature extends DefaultReconnectionFeature implements ICustomAbortableUndoRedoFeature {

		public boolean preUndoCalled = false;
		public boolean postUndoCalled = false;

		public boolean preRedoCalled = false;
		public boolean postRedoCalled = false;

		private IReconnectionContext context = null;
		private boolean abortAfterPreHook;

		public TestReconnectionFeature(IFeatureProvider fp, boolean abortAfterPreHook) {
			super(fp);
			this.abortAfterPreHook = abortAfterPreHook;
		}

		@Override
		public boolean canReconnect(IReconnectionContext context) {
			return true;
		}

		@Override
		public boolean isAbort() {
			return abortAfterPreHook;
		}

		@Override
		public boolean canUndo(IContext context) {
			return true;
		}

		@Override
		public void preUndo(IContext context) {
			preUndoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		@Override
		public void postUndo(IContext context) {
			postUndoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		public boolean canRedo(IContext context) {
			return true;
		}

		@Override
		public void preRedo(IContext context) {
			preRedoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		@Override
		public void postRedo(IContext context) {
			postRedoCalled = true;
			assertEquals("Context object must be the same as in execute", this.context, context);
		}

		public void preReconnect(IReconnectionContext context) {
			// Do nothing
			this.context = context;
		}

		public void postReconnect(IReconnectionContext context) {
		}

		public void canceledReconnect(IReconnectionContext context) {
		}
	}

	public class MockDiagramBehavior extends DiagramBehavior {

		private TransactionalEditingDomain editingDomain;

		public MockDiagramBehavior(IDiagramContainerUI diagramContainer, TransactionalEditingDomain editingDomain) {
			super(diagramContainer);
			this.editingDomain = editingDomain;
		}

		@Override
		public TransactionalEditingDomain getEditingDomain() {
			return editingDomain;
		}
	}

}
