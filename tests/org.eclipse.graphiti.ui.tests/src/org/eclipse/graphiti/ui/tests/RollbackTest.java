/*********************************************************************
* Copyright (c) 2005, 2022 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    mwenz - Bug 352220 - Possibility to disable guides
*    cbrand - Bug 377475 - Fix AbstractCustomFeature.execute and canExecute
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.net.URL;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.internal.EMFWorkspacePlugin;
import org.eclipse.emf.workspace.internal.EMFWorkspacePlugin.Implementation;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.internal.command.CommandContainer;
import org.eclipse.graphiti.internal.command.GenericFeatureCommandWithContext;
import org.eclipse.graphiti.mm.algorithms.AlgorithmsPackage;
import org.eclipse.graphiti.mm.algorithms.styles.StylesPackage;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramsPackage;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;
import org.eclipse.graphiti.tests.reuse.GFAbstractTestCase;
import org.eclipse.graphiti.ui.internal.command.GefCommandWrapper;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderInternal;
import org.eclipse.graphiti.ui.internal.editor.GFCommandStack;
import org.eclipse.graphiti.ui.platform.IConfigurationProvider;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 *
 */
public class RollbackTest extends GFAbstractTestCase {
	private static TransactionalEditingDomain editingDomain;

	@BeforeClass
	public static void before() {
		editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		ResourceSet resourceSet = editingDomain.getResourceSet();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("diagram", new XMIResourceFactoryImpl()); //$NON-NLS-1$

		// Register the packages of our model with EMF.
		{
			@SuppressWarnings("unused")
			Object o = PictogramsPackage.eINSTANCE;
			o = AlgorithmsPackage.eINSTANCE;
			o = StylesPackage.eINSTANCE;
		}
	}

	@AfterClass
	public static void after() {
		editingDomain.dispose();
	}

	@Test
	public void testRollback() {

		// Load diagram file.
		URL url = getClass().getClassLoader().getResource("org/eclipse/graphiti/ui/tests/tut.diagram"); //$NON-NLS-1$
		URI uri = URI.createURI(url.toString());
		ResourceSet resourceSet = editingDomain.getResourceSet();
		Resource diagramResource = resourceSet.getResource(uri, true);
		final Diagram diagram = (Diagram) diagramResource.getEObject("/0"); //$NON-NLS-1$

		// Prepare mocking.
		IToolBehaviorProvider toolBehaviorProvider = mock(IToolBehaviorProvider.class);

		IDiagramTypeProvider diagramTypeProvider = mock(IDiagramTypeProvider.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);
		when(featureProvider.getDiagramTypeProvider()).thenReturn(diagramTypeProvider);

		when(diagramTypeProvider.getCurrentToolBehaviorProvider()).thenReturn(toolBehaviorProvider);
		when(diagramTypeProvider.getFeatureProvider()).thenReturn(featureProvider);

		IConfigurationProvider configurationProvider = mock(IConfigurationProviderInternal.class);
		when(configurationProvider.getDiagramTypeProvider()).thenReturn(diagramTypeProvider);

		TestCustomFeature feature = new TestCustomFeature(configurationProvider.getFeatureProvider());
		ICustomContext context = mock(ICustomContext.class);
		
		try {
			Field pluginField = EMFWorkspacePlugin.class.getDeclaredField("plugin");
			pluginField.setAccessible(true);
			BundleContext bundleContextMock = mock(BundleContext.class);
			Bundle bundleMock = mock(Bundle.class);
			when(bundleMock.getSymbolicName()).thenReturn("org.eclipse.emf.workspace.plugin.id");
			when(bundleContextMock.getBundle()).thenReturn(bundleMock);
			Implementation impl = new Implementation();
			impl.start(bundleContextMock);
			pluginField.set(EMFWorkspacePlugin.class, impl);
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (NoSuchFieldException e1) {
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Execute feature to be rolled back
		GFCommandStack commandStack = new GFCommandStack(configurationProvider, editingDomain);
		GenericFeatureCommandWithContext featureCommand = new GenericFeatureCommandWithContext(feature, context);
		CommandContainer commandContainer = new CommandContainer(configurationProvider.getFeatureProvider());
		commandContainer.add(featureCommand);
		GefCommandWrapper commandWrapper = new GefCommandWrapper(featureCommand, editingDomain);
		
		commandStack.execute(commandWrapper);

		// Check.
		assertTrue("Rollback failed. SnapToGrid is false. ", diagram.isSnapToGrid());
		assertFalse(commandStack.isDirty());

	}

	class TestCustomFeature extends AbstractCustomFeature {

		public TestCustomFeature(IFeatureProvider fp) {
			super(fp);
		}

		@Override
		public boolean canExecute(ICustomContext context) {
			return true;
		}

		public void execute(ICustomContext context) {
			assertTrue(getDiagram().isSnapToGrid());
			getDiagram().setSnapToGrid(false);
			assertTrue(!getDiagram().isSnapToGrid());
			// force rollback
			throw new OperationCanceledException();
		}

		@Override
		public boolean hasDoneChanges() {
			return true;
		}
	}
}
