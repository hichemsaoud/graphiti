/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.ecore.features.pack;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.impl.DefaultMoveShapeFeature;

/**
 * The Class TestMovePackageFeature.
 */
public class TestMovePackageFeature extends DefaultMoveShapeFeature {

	/**
	 * Instantiates a new test move package feature.
	 * 
	 * @param fp
	 *            the fp
	 */
	public TestMovePackageFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {

		boolean ret = super.canMoveShape(context);

		// check further details only if move allowed by default feature
		if (ret) {

			// don't allow move if package name has the length of one
			Object bo = getBusinessObjectForPictogramElement(context.getShape());
			if (bo instanceof EPackage) {
				EPackage p = (EPackage) bo;
				if (p.getName() != null && p.getName().length() == 1) {
					ret = false;
				}
			}
		}
		return ret;
	}
}
