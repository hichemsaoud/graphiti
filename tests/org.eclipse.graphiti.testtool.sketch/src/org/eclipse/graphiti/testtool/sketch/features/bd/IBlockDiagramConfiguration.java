/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch.features.bd;

/**
 * The Interface IBlockDiagramConfiguration.
 */
public interface IBlockDiagramConfiguration {

	/**
	 * The Constant BD_CHANNEL.
	 */
	static final String BD_CHANNEL = "BD_CHANNEL";

	/**
	 * The Constant BD_ACTOR.
	 */
	static final String BD_ACTOR = "BD_ACTOR";

	/**
	 * The Constant BLOCK_DIAGRAM_ELEMENT_TYPE.
	 */
	static final String BLOCK_DIAGRAM_ELEMENT_TYPE = "BLOCK_DIAGRAM_ELEMENT_TYPE";

	/**
	 * The CHANNE l_ size.
	 */
	static int CHANNEL_SIZE = 20;

}
