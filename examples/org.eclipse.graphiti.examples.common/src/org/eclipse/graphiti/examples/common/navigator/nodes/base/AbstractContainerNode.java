/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.common.navigator.nodes.base;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * The Class AbstractContainerNode.
 */
public abstract class AbstractContainerNode implements IContainerNode {

	protected AbstractContainerNode() {
		super();
	}

	public String getText() {
		String ret = getContainerName();
		return ret;
	}

	/**
	 * Gets the container name.
	 * 
	 * @return the container name
	 */
	abstract protected String getContainerName();

	public boolean hasChildren() {
			return true;
	}

	public Image getImage() {
		String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
		return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
	}
}
